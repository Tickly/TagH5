module.exports = function () {
    var root = document.documentElement
        , cw = root.clientWidth
        , ch = root.clientHeight
        ;

    root.style.fontSize = cw / 10 + 'px';
};