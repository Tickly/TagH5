module.exports = function (grunt) {
    grunt.initConfig({
        less: {
            development: {
                src: 'src/less/tagh5.less',
                dest: 'public/css/tagh5.css'
            }
        },
        watch:{
            styles:{
                files:[
                    'src/less/*.less'
                ],
                tasks:[
                    'less:development'
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');


    grunt.registerTask('default', ['less:development']);
};